# Hi! 👋🏻 I'm Elli

```yaml
---
info:
  name: Elli
    translation: Έλλη
  full_name: Eleftheria
    translation: Ελευθερία
  pronouns: (she/her/hers)
```

## I'm an undergraduate student at Northwestern University :purple_heart:

I am...

- :books: studying Computer Science and Math.
- :robot: researching artificial life and evolutionary robotics in the [Xenobot Lab](https://www.xenobot.group/).
- 🧬 writing open-source synthetic biology software in the [Leonard Lab](https://www.leonard.northwestern.edu).
- :sparkling_heart: interested in algorithms, theoretical CS, low level programming,  robotics, machine learning, and artificial life.
- :speech_balloon: an advocate for free and open source software, technological decentralization, and the solarpunk movement.

---

### :hammer_and_wrench: Languages and Tools :

<div>
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/julia/julia-original.svg" title="Julia" alt="Julia" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/rust/rust-plain.svg" title="Rust" alt="Rust" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/ellifteria/ellifteria/main/static/c_logo.svg" title="Clang" alt="Clang" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/isocpp/logos/master/cpp_logo.svg" title="Cpp" alt="Cpp" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" title="Python" alt="Python" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/cython/cython/master/docs/_static/cython-logo-C.svg" title="Cython" alt="Cython" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/numpy/numpy-original.svg" title="Numpy" alt="Numpy" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jupyter/jupyter-original.svg" title="Jupyter" alt="Jupyter" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/matlab/matlab-original.svg" title="Matlab" alt="Matlab" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/raspberrypi/raspberrypi-original.svg" title="Raspberry Pi" alt="Raspberry Pi" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/arduino/arduino-original.svg" title="Arduino" alt="Arduino" width="40" height="40"/>&nbsp;
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg" title="Git" **alt="Git" width="40" height="40"/>
</div>

---

### 🏳️‍🌈 Love is love. 🏳️‍🌈

### ⚧️ Trans rights are human rights. ⚧️

### ♀️ Feminism is for everyone. ♀️

### 🌍 There is no planet B. 🌍

### 🔬 Science is real. 🔬

### ✊🏿 Black lives matter. ✊🏿
